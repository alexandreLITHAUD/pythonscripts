import requests
import os
import json
import sys
from datetime import datetime

REQUEST = "https://www.toptal.com/developers/gitignore/api/list?format=json"

def getJsonReponse():
    return requests.get(REQUEST)

def createFile(arr, baseName, path):

    if baseName:
        name = ".gitignore"
    else:
        now = datetime.now()
        name = now.strftime("gitignore_%b-%d-%Y_%H-%M-%S")

    try :
        if path != None:
            name = os.path.join(path,name)
        with open("{}".format(name), "w") as f:
            for a in arr:
                f.write(a)
    except Exception as e:
        print('An exception occurred: {}'.format(e))


def main(params, baseName, path):
    resp = getJsonReponse()
    jsonRep = resp.json()
    arr = []
    for p in params:

        if p == "vscode":
            arr.append(jsonRep.get("visualstudiocode").get("contents"))

        elif not (jsonRep.get(p) is None):
            arr.append(jsonRep.get(p).get("contents"))
        else:
            print("\n {} not present in the dataset \n".format(p))
            # TODO PROPOSE SIMILAR
    
    if len(arr) != 0:
        createFile(arr, baseName, path)

## TODO add -l and --list followed with the list of parameters (??)
if __name__ == "__main__":
    args = sys.argv
    if len(args) <= 1:
        raise Exception("Missing parameters")

    arr = []
    baseName = False
    path = None
    skipnext = False

    for i in range(len(args)):

        if skipnext:
            skipnext = False
            continue

        if args[i] == "-t" or args[i] == "--target":
            path = args[i+1]
            skipnext = True
        elif args[i] == "-b" or args[i] == "--base-name":
            baseName = True
        else:
            arr.append(args[i])

    params = [x.lower() for x in arr[1:]]

    main(params, baseName, path)