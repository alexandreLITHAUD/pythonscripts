# Gitignore Generator

## Summary

A Script that will use the toptal gitignore api in order to create a good .gitignore that will ignore waste files of some technologies.

## Usage

The commands :
- **-b** or **--base-name** : create the file with the .gitignore name
- **-t \<path>** or **--target \<path>**  : will create the file in the targeted path

Usage example :
- `python3 gitignore_generator.py linux macos vscode`
- `python3 gitignore_generator.py -b -t . linux macos python`

If the name wanted is not found by the program it will show on the terminal and craete a file with the rest of the found parameters.

## Librairies

- os
- **requests** (not installed by default)
- os
- json
- sys
- datetime


## Installation

### Nix

Use the command : `nix develop` to create the temporary environnement with python3 and all the librairies.

### Non Nix

Download the request librairy with this command (For Python 3.7+) : `python -m pip install requests`
