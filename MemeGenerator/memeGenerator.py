import requests
from PIL import Image
from io import BytesIO
import sys

REQUEST = "https://meme-api.com/gimme"

def main():
    memeRep = requests.get(REQUEST).json()
    responseImage = requests.get(memeRep["preview"][-1]);
    with Image.open(BytesIO(responseImage.content)) as test:
        test.show()

if __name__ == "__main__":
    argv = sys.argv
    if len(argv) > 1:
        REQUEST += "/" + argv[1]
    main()
