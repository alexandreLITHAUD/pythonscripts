{
    description = "Main flake for each python Scripts";

    inputs = 
    {
        flake-utils.url = "github:numtide/flake-utils";
        gitignoreGenerator.url = "./GitignoreGenerator";
        newsChecker.url = "./NewsChecker";
    };

    outputs = { self, flake-utils, gitignoreGenerator}:
        flake-utils.lib.eachDefaultSystem
        (system: {
        devShells = {
            gitignore_generator = gitignoreGenerator.devShells.${system}.default;
            news_checker = newsChecker.devShells.${system}.default;
        };
    });  

}