# Python-Scripts
> @alexandreLITHAUD

## Summary

- This project is sum of multiple little python script. You can be use or modified by anyone.

- Feel free to add your own liitle python script by proposing a pull request (*each will be verified*)

- All python script directory must have the needed library that will need to be installed , a quick documentation on how to use each program as well as a quick summary of what the script is doing.

- **Every script has a nix file in order for nix user to easily create a correct environement**

## List of Scripts :

- **[Gitignore Generator](./GitignoreGenerator/)** : [README.md](./GitignoreGenerator/README.md)
- **[News Checker](./NewsChecker/)** : [README.md](./NewsChecker/README.md)
- **TODO**

## Installation

### For NIX User

You can easily launch any of the program by creating a nix-shell with all the wanted depedencies.

- In this folder : `nix develop .#<program name>`

- Or directly in the correct package : `nix develop`

### For Non Nix User (pip)

You can install all the needed packages that are used by the script by following the Installation part in the README.md oof the wanted script.