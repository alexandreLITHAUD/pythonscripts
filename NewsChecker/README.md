# News Checker

# Note

You will have to create an account to the News API website : https://newsapi.org/ in order to get an API key for the script to work.(It is free).

Once you got it create in this folder a file named `apikey` an paste it in here. Whith all that the app will be able to use your own api key in order to use the api. (You will be limited to 100 requests per day)

## Summary

This little script will get news that are shared by the news api and store it in a readable markdown file

## Usage

- **-k \<keyword>** or **--keyword \<keyword>** : Will only search for news with this particuliar keyword
- **-l \<language>** or **--language \<language>** (using iso notation) : Will search for news that are in that articuliar language
- **-h** or **--head** : Will search for news that are the more recent
- **-e** or **--every** : Will searcg for all the news

Usage example :
- `python3 news_checker.py -h -l fr`
- `python3 news_checker.py -e -k test -l us`

-h or -e must be chosen

## Librairies

- os
- **requests** (not installed by default)
- os
- json
- sys
- datetime
- Enum

## Installation

### Nix

Use the command : `nix develop` to create the temporary environnement with python3 and all the librairies.

### Non Nix

Download the request librairy with this command (For Python 3.7+) : `python -m pip install requests`
