import requests
import os
import json
import sys
from enum import Enum
from datetime import datetime

# Requets for the main news (need country and apikey)
REQUEST_HEAD = "https://newsapi.org/v2/top-headlines?country=@&apiKey=@"
# Requets fir every thing (need country keword and apikey)
REQUEST_EVERY = "GET https://newsapi.org/v2/everything?apiKey=@&language=@"

# The above code is defining an enumeration class called WantedType.
class WantedType(Enum):
    HEAD = "HEAD"
    EVERY = "EVERY"

def getAPIKey():
    """
    This function reads an API key from a file named "apikey" and returns it, or raises an exception if
    the file is empty.
    :return: the API key read from the "apikey" file. If the file is empty or does not exist, it will
    raise an exception with the message "no data was found in the apikey file".
    """
    res = ""
    try:
        # TODO CHANGE TO USE OS.PATH
        fp = open("./apikey", 'r')
        res = fp.readline()
    finally:
         fp.close()

    if res=="":
         raise Exception("no data was found in the apikey file")
    return res
     
def checkAPIKeyFile():
    """
    The function checks if a file named "apikey" exists in the current directory and returns a boolean
    value.
    :return: a boolean value indicating whether a file named "apikey" exists in the current directory or
    not. If the file exists, the function will return True, otherwise it will return False.
    """
    return os.path.isfile("apikey")

def appendKeyWord(rq, keyword):
    """
    The function appends a keyword to a given request string.
    
    :param rq: The parameter "rq" is a string that represents a search query. It may already contain
    some search parameters, and this function is designed to append a new keyword to the query
    :param keyword: The keyword parameter is a string that represents a search term or keyword that will
    be appended to the end of the rq (request) parameter
    :return: the updated value of the `rq` parameter after appending the `keyword` parameter to it. If
    the `keyword` parameter is an empty string, the function returns the original value of `rq` without
    any changes.
    """
    if keyword == "":
          return rq
    else:
         rq += "&q={}".format(keyword)
         return rq

def getjsonResponse(wt,keyword="",country="fr"):
    """
    This function takes in parameters for a wanted type, keyword, and country, and returns a JSON
    response based on those parameters.
    
    :param wt: WantedType enum object, which specifies the type of response wanted (either HEAD or
    EVERY)
    :param keyword: The keyword parameter is a string that represents a search term or query that will
    be used to filter the results of the API request. It is an optional parameter, so if no keyword is
    provided, the API will return all results that match the other specified parameters
    :param country: The country parameter is a two-letter country code that specifies the country for
    which the API request is being made. It is used in the API request URL to retrieve data specific to
    that country, defaults to fr (optional)
    :return: a response object from the requests library based on the input parameters. The response
    object contains the server's response to the HTTP request made by the function.
    """
    if(wt == WantedType.HEAD):
        rq = REQUEST_HEAD.replace("@",str(country),1)
        rq = rq.replace("@",str(getAPIKey()),1)
        return requests.get(rq)
    elif(wt == WantedType.EVERY):
        rq = REQUEST_EVERY.replace("@", str(getAPIKey()),1)
        rq = rq.replace("@",str(country),1)
        rq = appendKeyWord(rq, keyword)
        return rq
    else:
        raise Exception("Should not happen")
    

def generateHeaderData(jsonRep):
    """
    This function generates a header for a news checker program that includes the number of articles and
    links to each article's title.
    
    :param jsonRep: The parameter `jsonRep` is expected to be a JSON representation of news articles,
    containing information such as the total number of articles and details about each individual
    article
    :return: The function `generateHeaderData` returns a string that contains the header data for a news
    checker application. The string includes the title of the application, the total number of articles,
    and a list of article titles with links to their corresponding sections in the application.
    """
    print("here")
    str = ""
    str += "# NEWS CHECKER\n"
    str += "There are {} articles\n\n".format(jsonRep["totalResults"])
    for article in jsonRep["articles"]:
        str += "- [{}](#{})\n".format(article["title"], article["title"].replace(" ","-").lower())
    return str

def genereateBodyData(jsonRep):
    """
    The function generates a formatted string containing information from a JSON object representing
    news articles.
    
    :param jsonRep: The parameter `jsonRep` is expected to be a JSON object that contains a list of
    articles, where each article is represented as a JSON object with properties such as `title`,
    `description`, `author`, `url`, `publishedAt`, and `content`. The function `generateBodyData`
    :return: a string that contains formatted data extracted from a JSON representation of articles. The
    string includes the title, description, author, URL, published date, and content of each article.
    """
    print("here2")
    str = ""
    for article in jsonRep["articles"]:
        str += "## {}\n".format(article["title"])
        str += "**Description : {}**\n\n".format(article["description"])
        str += "Author : {}\n\n".format(article["author"])
        str += "Url : {}\n\n".format(article["url"])
        str += "Published Date : {}\n\n".format(article["publishedAt"])
        str += "Content : {}\n\n".format(article["content"])
    return str

def generateHTMLFile(jsonRep):
    """
    This function generates an HTML file with a header and body data based on a given JSON
    representation.
    
    :param jsonRep: The parameter `jsonRep` is likely a JSON representation of news articles or data
    that will be used to generate an HTML file. The function `generateHTMLFile` takes this JSON
    representation and writes it to an HTML file with a filename based on the current date. The function
    also calls two other functions
    """
    now = datetime.now()
    name = now.strftime("News_%b-%d-%Y")

    try:
        # TODO CHANGE TO USE OS.PATH
        with open("./{}.md".format(name),"w") as f:
            f.write(generateHeaderData(jsonRep))
            f.write(genereateBodyData(jsonRep))
    except Exception as e:
        print('An exception occurred: {}'.format(e))
    finally:
        f.close()


def main(params, wt, language, keyword):
    """
    The function takes in parameters and generates an HTML file based on a JSON response.
    
    :param params: It is not clear what the `params` parameter is used for in this code snippet. It is
    likely that it is used in other parts of the code that are not shown here
    :param wt: It is not clear what "wt" stands for without more context. It could be an abbreviation
    for a variable or a parameter used in the code
    :param language: The language parameter is likely a string that specifies the programming language
    being used in the code. It may be used in the getjsonResponse function to retrieve relevant
    information or in the generateHTMLFile function to format the output appropriately
    :param keyword: The keyword parameter is likely a string that represents a search term or topic that
    is being used to retrieve data from an API or other data source. It is used as a parameter for the
    getjsonResponse function to retrieve relevant data
    """
    resp = getjsonResponse(wt, keyword,language)

    jsonRep = resp.json()

    if jsonRep["status"] == "error":
        print(jsonRep)
        raise Exception("Request error")

    # jsonRep = json.load(resp)

    generateHTMLFile(jsonRep)


if __name__ == "__main__":

    if not checkAPIKeyFile():
         raise Exception ("No api key file found")
    
    args = sys.argv
    if len(args) <= 1:
            raise Exception("Missing parameters")

    arr = []
    skipnext = False
    keyword = ""
    language = ""
    wt = None
    for i in range(len(args)):
         
        if skipnext:
            skipnext = False
            continue

        if args[i] == "-k" or args[i] == "--keyword":
            skipnext = True
            keyword = args[i+1]
        elif args[i] == "-l" or args[i] == "--language":
            skipnext = True
            language = args[i+1]
        elif args[i] == "-h" or args[i] == "--head":
            wt = WantedType.HEAD
        elif args[i] == "-e" or args[i] == "--every":
            wt = WantedType.EVERY
        else:
            arr.append(args[i])

    params = [x.lower() for x in arr[1:]]

    if wt == None:
        raise Exception("-h or -e needed")

    main(params, wt, language, keyword)